# Ensembl Technical Test

Please see the live demo at https://ensembl.vercel.app/.

## Approach

* The project structure is based on `create-react-app` as a standard way to get started.
* I considered that the diagram looks a bit like a Gantt chart and I wanted to try out the "visx" library from Airbnb (https://github.com/airbnb/visx), so I searched for an example and found this: https://codesandbox.io/s/yjol7n7qq9. It forms the basis of the `Browser` component.
* I've had some success with `AutoSizer` from `react-virtualized` in the past, so I have used it to provide dimensions to the diagram here. The "render prop" style is somewhat out of fashion as I understand, but it works.
* I've used a reducer pattern for the main state because it's easy to manage multiple state changes resulting from one action. For example, setting the current gene and adding a history item.
* The "middleware" is based on a Redux pattern for async actions that I've used in the past.
* I have not used static action types or actions creators for simplicity
* The search text has been left as local state as it would probably not need to be persisted.
* There are a few Tailwind-like css classes added for layout purposes. I've always thought that Tailwind could be written by hand on an as-needed basis and I enjoy the "Utility CSS" style.

## Shortcomings

* Whilst I am familiar with TypeScript, I have never used it with React so I decided not to use it here.
* I would normally lean heavily on automatic formatting, but since it didn't work out of the box with `create-react-app`/VS Code, I didn't spend time on it. Please excuse any minor formatting issues.
* Lastly, and most regrettably, I have not included any tests. I admit that I'm not very experienced with UI tests, but I could test the reducer in isolation or use something like Storybook to enumerate states of the `Browser` component and execute them in CI.
