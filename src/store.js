import { useReducer } from 'react';

const middleware = (dispatch) => (action = {}) => {
  if (action.promise) {
    dispatch({ type: `${action.type}_ATTEMPT` })
    action.promise
      .then(result => dispatch({ type: `${action.type}_SUCCESS`, result }))
      .catch(error => dispatch({ type: `${action.type}_FAILURE`, error }))
  } else {
    dispatch(action)
  }
}

const initialState = {
  loading: false,
  error: null,
  gene: null,
  sort: 'none',
  history: [],
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SUBMIT_ATTEMPT':
      return {
        ...state,
        loading: true,
        error: null,
      }
    case 'SUBMIT_FAILURE':
      return {
        ...state,
        loading: false,
        error: action.error.message
      }
    case 'SUBMIT_SUCCESS': {
      const { result } = action
      let nextState = { ...state, loading: false }
      if (result.error) {
        return {
          ...nextState,
          error: result.error
        }
      }
      if (result.object_type !== 'Gene') {
        return {
          ...nextState,
          error: `Not a gene: ${result.object_type}`
        }
      }
      return {
        ...nextState,
        gene: result,
        history: [
          result,
          ...state.history.slice(0, 4)
        ]
      }
    }
    case 'SET_GENE':
      return {
        ...state,
        gene: action.payload
      }
    case 'SET_SORT':
      return {
        ...state,
        sort: action.payload
      }
    default:
      return state
  }
}

const useStore = () => {
  const [state = initialState, _dispatch] = useReducer(reducer)
  const dispatch = middleware(_dispatch)

  return [state, dispatch]
}

export default useStore
