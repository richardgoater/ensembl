// import { useMemo } from 'react';
import { Group } from '@visx/group';
import { AxisLeft, AxisBottom } from '@visx/axis';
import { scaleLinear, scaleBand } from '@visx/scale';

const defaultMargin = {
  top: 24,
  left: 120,
  right: 24,
  bottom: 48
}

const sortTranscripts = (transcripts, sort) => {
  if (sort === 'none') {
    return transcripts
  }
  return [ ...transcripts ].sort((a, b) => {
    const aLen = a.end - a.start
    const bLen = b.end - b.start
    if (aLen > bLen) return sort === 's2l' ? 1 : -1
    if (aLen < bLen) return sort === 's2l' ? -1 : 1
    return 0
  })
}

const Browser = ({ gene, sort, width = 300, height = 150, margin = defaultMargin }) => {
  const { start, end, Transcript } = gene

  const sortedTranscripts = sortTranscripts(Transcript, sort)

  var x = scaleLinear()
    .domain([ start, end ]).nice()
    .range([0, width - margin.left - margin.right]);

  var y = scaleBand()
    .domain(sortedTranscripts.map(t => t.id))
    .rangeRound([0, height - margin.top - margin.bottom])
    .padding(0.5);

  return (
    <svg width={width} height={height} fontSize="10px" fontFamily="sans-serif" shapeRendering="crispEdges">
      <Group top={margin.top} left={margin.left} key={sort}>
        {sortedTranscripts.map(t =>
          <Group key={t.id}>
            <line
              x1={x(t.start)}
              y1={y(t.id) + y.bandwidth() / 2}
              x2={x(t.end)}
              y2={y(t.id) + y.bandwidth() / 2}
              stroke="black"
              strokeWidth="1px"
            />
            {t.Exon.map(e => (
              <rect
                key={e.id}
                x={x(e.start)}
                y={y(t.id)}
                width={x(e.end) - x(e.start)}
                height={y.bandwidth()}
                fill="lightgrey"
                stroke="black"
                strokeWidth="1px"
              />
            ))}
          </Group>
        )}
        <AxisLeft
          top={0}
          left={x(0)}
          scale={y}
          hideTicks
          numTicks={Transcript.length}
          tickLabelProps={() => ({
            textAnchor: 'end',
            fontSize: 10,
            fontFamily: 'Arial',
            x: -x(0) - 8,
            dx: '-0.25em',
            dy: '0.25em',
          })}
        />
        <AxisBottom
          scale={x}
          top={height - margin.top - margin.bottom}
        />
      </Group>
    </svg>
  )
}

export default Browser
