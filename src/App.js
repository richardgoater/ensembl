import './App.css';

import { useState } from 'react';
import AutoSizer from 'react-virtualized-auto-sizer'

import Browser from './Browser'

import useStore from './store'

function App() {
  const [state, dispatch] = useStore()

  const [searchText, setSearchText] = useState('ENSG00000157764')

  const handleSubmit = (e) => {
    e.preventDefault()
    dispatch({
      type: 'SUBMIT',
      promise: fetch(
        `https://rest.ensembl.org/lookup/id/${searchText}?content-type=application/json;expand=1`
      )
      .then(response => response.json())
    })
  }

  const { loading, error, gene, sort, history } = state

  return (
    <>
      <h1>Ensembl</h1>
      <div className="flex w-full h-full">
        <div className="w-3/4 flex flex-col">
          <form onSubmit={handleSubmit}>
            { error && <p>{error}</p> }
            <input
              type="text"
              value={searchText}
              onChange={e => setSearchText(e.target.value)}
            />
            <button>submit</button>
            { loading && <p>loading...</p> }
          </form>
          <form>
            <p>Sort:</p>
            <label className="block">
              <input
                type="radio"
                value=""
                checked={sort === 'none'}
                onChange={() => dispatch({ type: 'SET_SORT', payload: 'none' })}
              />
              none
            </label>
            <label className="block">
              <input
                type="radio"
                value="l2s"
                checked={sort === 'l2s'}
                onChange={() => dispatch({ type: 'SET_SORT', payload: 'l2s' })}
              />
              longest to shortest
            </label>
            <label className="block">
              <input
                type="radio"
                value="s2l"
                checked={sort === 's2l'}
                onChange={() => dispatch({ type: 'SET_SORT', payload: 's2l' })}
              />
              shortest to longest
            </label>
          </form>
          <div className="h-full">
            { gene &&
              <AutoSizer>
                { size => <Browser {...size} gene={gene} sort={sort} /> }
              </AutoSizer> }
          </div>
        </div>
        <div>
          <h2>History</h2>
          <ul>
            { history.map(gene =>
              <li key={gene.id}>
                <button onClick={() => dispatch({ type: 'SET_GENE', payload: gene })}>
                  {gene.id}
                </button>
              </li> )}
          </ul>
        </div>
      </div>
    </>
  );
}

export default App;
